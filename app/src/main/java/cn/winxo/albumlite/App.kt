package cn.winxo.albumlite

import android.app.Application

/**
 * @author yunxu
 * @date 29/03/2018
 * @desc
 */
class App : Application() {

  override fun onCreate() {
    super.onCreate()
  }
}
