package cn.winxo.albumlite.module.contract

import cn.winxo.albumlite.base.ibase.IPresenter
import cn.winxo.albumlite.base.ibase.IView
import me.drakeet.multitype.Items

interface MainContract {

  interface View : IView {
    fun showTools(items: Items)
  }

  interface Presenter : IPresenter<View> {
    fun loadToolItem()
  }
}