package cn.winxo.albumlite.module.view

import android.support.v7.widget.GridLayoutManager
import cn.winxo.albumlite.R
import cn.winxo.albumlite.adapter.ToolViewBinder
import cn.winxo.albumlite.base.BaseMvpActivity
import cn.winxo.albumlite.module.contract.MainContract
import cn.winxo.albumlite.module.contract.MainContract.Presenter
import cn.winxo.albumlite.module.model.entity.Tool
import cn.winxo.albumlite.module.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.recycler
import me.drakeet.multitype.Items
import me.drakeet.multitype.MultiTypeAdapter

class MainActivity : BaseMvpActivity<MainContract.Presenter>(), MainContract.View {

  private lateinit var multiTypeAdapter: MultiTypeAdapter

  override fun onLoadPresenter(): Presenter {
    return MainPresenter(this)
  }

  override fun setLayoutResourceID(): Int {
    return R.layout.activity_main
  }

  override fun initView() {
    recycler.layoutManager = GridLayoutManager(this, 3)
    multiTypeAdapter = MultiTypeAdapter()
    multiTypeAdapter.register(Tool::class.java, ToolViewBinder())
    recycler.adapter = multiTypeAdapter
  }

  override fun initData() {
    super.initData()
    mPresenter.loadToolItem();
  }

  override fun showTools(items: Items) {
    multiTypeAdapter.items = items
    multiTypeAdapter.notifyDataSetChanged()
  }
}
