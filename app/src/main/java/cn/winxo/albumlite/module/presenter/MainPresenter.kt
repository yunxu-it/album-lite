package cn.winxo.albumlite.module.presenter

import android.content.Context
import cn.winxo.albumlite.base.BasePresenter
import cn.winxo.albumlite.module.contract.MainContract
import cn.winxo.albumlite.module.model.entity.Tool
import me.drakeet.multitype.Items

class MainPresenter(
    private val mContext: Context) : BasePresenter<MainContract.View>(), MainContract.Presenter {

  override fun loadToolItem() {
    var items = Items()
    for (i in 1..3) {
      println(i)
      items.add(Tool("icon" + i, "name" + i))
    }
    mView?.showTools(items)
  }
}
