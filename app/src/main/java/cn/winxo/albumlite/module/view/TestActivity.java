package cn.winxo.albumlite.module.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import cn.winxo.albumlite.adapter.ToolViewBinder;
import cn.winxo.albumlite.module.model.entity.Tool;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * @author lxlong
 * @date 2018/5/5
 * @desc
 */
public class TestActivity extends AppCompatActivity {

  private int number;
  private MultiTypeAdapter mTypeAdapter;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mTypeAdapter = new MultiTypeAdapter();
    mTypeAdapter.register(Tool.class, new ToolViewBinder());
  }
}
