package cn.winxo.albumlite.base

import cn.winxo.albumlite.base.ibase.IPresenter
import cn.winxo.albumlite.base.ibase.IView

abstract class BasePresenter<V : IView> : IPresenter<V> {

  protected var mView: V? = null

  override fun attachView(view: V) {
    mView = view
  }

  override fun detachView() {
    mView = null
  }

  override fun onUnSubscribe() {
  }
}