package cn.winxo.albumlite.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * @author lxlong
 * @date 22/04/2018
 * @desc
 */
abstract class BaseActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    init(savedInstanceState)
    setContentView(setLayoutResourceID())
    initLate(savedInstanceState)
    initPresenter()
    initView()
    initData()
  }

  protected fun handleRxMsg(`object`: Any) {}

  protected fun init(savedInstanceState: Bundle?) {}

  protected abstract fun setLayoutResourceID(): Int

  protected fun initLate(savedInstanceState: Bundle?) {}

  protected open fun initPresenter() {}

  protected abstract fun initView()

  protected open fun initData() {}
}
