package cn.winxo.albumlite.base.ibase

/**
 * @author lxlong
 * @date 2018/5/4
 * @desc
 */
interface IPresenter<in T : IView> {

  fun attachView(view: T)

  fun detachView()

  /**
   * RxJava取消注册，以避免内存泄露
   */
  fun onUnSubscribe()
}