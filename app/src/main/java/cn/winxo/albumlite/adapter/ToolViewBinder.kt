package cn.winxo.albumlite.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import cn.winxo.albumlite.R
import cn.winxo.albumlite.module.model.entity.Tool
import me.drakeet.multitype.ItemViewBinder

/**
 * @author lxlong
 * @date 2018/5/5
 * @desc
 */
class ToolViewBinder : ItemViewBinder<Tool, ToolViewBinder.ViewHolder>() {


  override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
    val root = inflater.inflate(R.layout.item_tool, parent, false)
    return ViewHolder(root)
  }

  override fun onBindViewHolder(holder: ViewHolder, tool: Tool) {
    //holder.mToolImage.
    holder.mToolName.text = tool.name
  }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var mToolName: TextView = itemView.findViewById(R.id.tool_name)
    var mToolImage: ImageView = itemView.findViewById(R.id.tool_image)
  }
}
